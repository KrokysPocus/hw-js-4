const firstNumber = parseFloat(prompt('Введіть перше число:'));
const secondNumber = parseFloat(prompt('Введіть друге число:'));
const mathOperation = prompt('Введіть наступні знаки:"+", "-", "*", "/"');

function calculate (firstNumber, secondNumber, mathOperation) {
    let result;
    switch (mathOperation) {
        case "+":
            result = firstNumber + secondNumber;
            break;
        case "-":
            result = firstNumber - secondNumber;
            break;
        case "*":
            result = firstNumber * secondNumber;
            break;
        case "/":
            result = firstNumber / secondNumber;
            break;
        default:
            console.log("Не вірна команда!");
            return;
    }
    return result;
}

const result = calculate(firstNumber, secondNumber, mathOperation);
console.log(result);